import org.json.JSONException;
import org.json.JSONObject; 
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Pattern;
import java.util.regex.Matcher; 
import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
 
public class FetchTicker {
 
	public final String USER_AGENT = "Mozilla/5.0";
	static String ticker = "";
	public static void main(String[] args) throws Exception {
 
		FetchTicker http = new FetchTicker();
 
		//System.out.println("Testing 1 - Send Https GET request");
		//http.sendGet();
 
		System.out.println("\nTesting 2 - Send Http POST request");
		String tick=http.sendPost();
 
	}
  
	// HTTP POST request
	public String sendPost() throws Exception {
 		String data[];
		String url = "http://www.ncdex.com/ScriptService.asmx/GetLatestSpotIndex";
		URL obj = new URL(url);
		System.setProperty("jsse.enableSNIExtension", "false");
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json;charset=utf-8");
        	//con.setRequestProperty("Authorization", "key=    AIzaSyB-Cqnj23O1Gv1JQP3c2r9ydINhCmeOmBQ");
		//String urlParameters = "data.title=swapnil&data.message=udapure&registration_id=APA91bHgeA6xYIAHB6pHwm6esczTwr93G75suozQX0MUl1yudz49wRAvUd83NF-0ys_W2T544-DhpUUMapGun0LGLF037XUzbwp8l9ylCHx6YmqxBgba-u0rIUz5Aa59KDaXogeD_8k3wPzo3UGw20uxCp45oXyj0A";
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes("{}");
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		//System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		String response =""; //new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response+=inputLine;
		}
		in.close();
 		JSONObject job = new JSONObject(response);
		response = job.getString("d");
		//print result
		String[] msg = response.toString().split("\\|");
		String tabl = "";
		for(String s : msg){
		tabl+=removeUTFCharacters(s);

	        }
		//System.out.println(tabl);

Document doc = null;
  //try {
   doc = Jsoup.parse(tabl);
   
  //} 
int i=0;
     for (Element table : doc.select("table")) {
         for (Element row : table.select("tr")) {
		for(Element tds : row.select("td")){
             Elements tdss = row.select("td");
             ticker+=tdss.get(i).text()+" | ";
                 //System.out.println(tdss.get(i).text());

		i++;
		}
         }
     }
		
 //System.out.println(ticker);
 return ticker;
	}
public static StringBuffer removeUTFCharacters(String data){
Pattern p = Pattern.compile("\\\\u(\\p{XDigit}{4})");
Matcher m = p.matcher(data);
StringBuffer buf = new StringBuffer(data.length());
while (m.find()) {
String ch = String.valueOf((char) Integer.parseInt(m.group(1), 16));
m.appendReplacement(buf, Matcher.quoteReplacement(ch));
}
m.appendTail(buf);
return buf;
}
 
}
