import org.json.JSONException;
import org.json.JSONObject; 
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import javax.net.ssl.HttpsURLConnection;
 
public class HttpURLConnectionExample {
 
	private final String USER_AGENT = "Mozilla/5.0";
 
	public static void main(String[] args) throws Exception {
 
		HttpURLConnectionExample http = new HttpURLConnectionExample();
 
		//System.out.println("Testing 1 - Send Https GET request");
		//http.sendGet();
 
		System.out.println("\nTesting 2 - Send Http POST request");
		http.sendPost();
 
	}
 
	// HTTP GET request
	private void sendGet() throws Exception {
 
		String url = "http://192.168.0.35:8080/appoint/hl.jsp?name=dfdf";
 
		URL obj = new URL(url);
		System.setProperty("jsse.enableSNIExtension", "false");
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		// optional default is GET
		con.setRequestMethod("GET");
 
		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
 
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		String response = "";
 
		while ((inputLine = in.readLine()) != null) {
			response+=inputLine;
		}
		//in.close();
 
		//print result
		System.out.println(response);
 
	}
 
	// HTTP POST request
	private void sendPost() throws Exception {
 		String data[];
		String url = "https://android.googleapis.com/gcm/send";
		URL obj = new URL(url);
		System.setProperty("jsse.enableSNIExtension", "false");
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json");
        	con.setRequestProperty("Authorization", "key=    AIzaSyB-Cqnj23O1Gv1JQP3c2r9ydINhCmeOmBQ");
		
		JSONObject jobj1=new JSONObject();
			
			try
			{
			
			
			JSONObject data1=new JSONObject();
			data1.put("title","swapnil");
			data1.put("message","udapure");
			jobj1.put("data",data1);


			
			
			JSONArray ja=new JSONArray();
			ja.put("APA91bHr7NLSaVIbbLAfbMY6uWphQwNTqLYE0-JjndNmjkvj3ZgILvXQOm-u0MSg-zIEH-2ZYkIHOlrjbRw20fzTeJxJ6lpX88u9C7EQQLCfofgTo4N07BrqrSq37s3KYJ9Btq4vrTO2axRr6lHrnMfsB_OU7KeasQ");
			jobj1.put("registration_id",ja);
			
			
			} 
			catch (JSONException e) 
			{
            e.printStackTrace();
			}
			
		String urlParameters = "data.title=swapnil&data.message=udapure&registration_id=APA91bHr7NLSaVIbbLAfbMY6uWphQwNTqLYE0-JjndNmjkvj3ZgILvXQOm-u0MSg-zIEH-2ZYkIHOlrjbRw20fzTeJxJ6lpX88u9C7EQQLCfofgTo4N07BrqrSq37s3KYJ9Btq4vrTO2axRr6lHrnMfsB_OU7KeasQ";
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println(response.toString());
		System.exit(0);
		data = response.toString().split("::");
		//System.out.println(data[0]);
		//System.out.println(data[1]);
		int count = 1;
		try {
		JSONObject jobj = new JSONObject(data[0].trim());
             	int cnt=Integer.parseInt(data[1].trim());
		while(count<=cnt)
		{
		System.out.print(jobj.getString("commo_name"+count));
             	System.out.print(":"+jobj.getString("count"+count));
             	System.out.print(":"+jobj.getString("commo_id"+count));
//jobj.remove(jobj.getString("commo_name"+cnt));jobj.remove(jobj.getString("count"+cnt));jobj.remove(jobj.getString("commo_id"+cnt));
		count++;
		System.out.print("\n");
		} 
       		 } catch (JSONException e) {
      	 	     e.printStackTrace();
      		  }
 
	}
 
}
